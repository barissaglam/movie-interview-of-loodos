package barissaglam.client.loodosinterview.data.response

import barissaglam.client.loodosinterview.data.Movie
import com.google.gson.annotations.SerializedName
import kotlin.math.roundToInt

data class SearchResponse(
    @SerializedName("Response") val response:Boolean?,
    @SerializedName("Search") val movies: ArrayList<Movie>?,
    @SerializedName("totalResults") val totalResults: Int?
){
    fun getTotalPage(): Int = ((totalResults ?: 0 / 20) + 0.5).roundToInt()
}