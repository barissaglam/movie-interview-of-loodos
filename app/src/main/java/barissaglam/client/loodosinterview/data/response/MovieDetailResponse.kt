package barissaglam.client.loodosinterview.data.response

import barissaglam.client.loodosinterview.data.Rating
import com.google.gson.annotations.SerializedName

data class MovieDetailResponse(
    @SerializedName("Title") val title: String? = null,
    @SerializedName("Poster") val poster: String? = null,
    @SerializedName("Year") val year: String? = null,
    @SerializedName("Released") val released: String? = null,
    @SerializedName("Runtime") val runtime: String? = null,
    @SerializedName("Genre") val genre: String? = null,
    @SerializedName("Writer") val writer: String? = null,
    @SerializedName("Actors") val actors: String? = null,
    @SerializedName("Plot") val plot: String? = null,
    @SerializedName("Language") val language: String? = null,
    @SerializedName("Awards") val awards: String? = null,
    @SerializedName("imdbRating") val imdbRating: String? = null,
    @SerializedName("imdbVotes") val imdbVotes: String? = null,
    @SerializedName("Type") val type: String? = null,
    @SerializedName("Ratings") val ratings: ArrayList<Rating>? = null
)