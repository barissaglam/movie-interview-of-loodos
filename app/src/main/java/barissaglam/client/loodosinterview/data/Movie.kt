package barissaglam.client.loodosinterview.data

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Movie(
    @SerializedName("Title") val title:String?=null,
    @SerializedName("Year") val year:String?=null,
    @SerializedName("imdbID") val imdbID:String?=null,
    @SerializedName("Type") val type:String?=null,
    @SerializedName("Poster") val poster:String?=null
):Parcelable{

}