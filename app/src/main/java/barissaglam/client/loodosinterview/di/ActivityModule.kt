package barissaglam.client.loodosinterview.di

import barissaglam.client.loodosinterview.ui.detail.MovieDetailActivity
import barissaglam.client.loodosinterview.ui.home.HomeActivity
import barissaglam.client.loodosinterview.ui.splash.SplashActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityModule {

    @ContributesAndroidInjector
    abstract fun bindSplashActivity(): SplashActivity

    @ContributesAndroidInjector
    abstract fun bindHomeActivity(): HomeActivity

    @ContributesAndroidInjector
    abstract fun bindMovieDetailActivity(): MovieDetailActivity


}