package barissaglam.client.loodosinterview.di

import barissaglam.client.loodosinterview.BuildConfig
import barissaglam.client.loodosinterview.R
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module(
    includes = [
        ViewModelModule::class
    ]
)
class AppModule {

    @Provides
    @Singleton
    fun getFirebaseRemoteConfig(): FirebaseRemoteConfig {
        val mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance()
        mFirebaseRemoteConfig.setDefaultsAsync(R.xml.remote_config_defaults)
        val cacheExpiration = if (BuildConfig.DEBUG) 0.toLong() else 3600.toLong()
        val configSettings = FirebaseRemoteConfigSettings.Builder()
            .setMinimumFetchIntervalInSeconds(cacheExpiration)
            .build()
        mFirebaseRemoteConfig.setConfigSettingsAsync(configSettings)
        return mFirebaseRemoteConfig
    }

}