package barissaglam.client.loodosinterview.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import barissaglam.client.loodosinterview.base.viewmodel.ViewModelFactory
import barissaglam.client.loodosinterview.ui.detail.MovieDetailViewModel
import barissaglam.client.loodosinterview.ui.home.HomeViewModel
import barissaglam.client.loodosinterview.ui.splash.SplashViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {

    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(SplashViewModel::class)
    abstract fun bindsSplashViewModel(viewModel: SplashViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(HomeViewModel::class)
    abstract fun bindsHomeViewModel(viewModel: HomeViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MovieDetailViewModel::class)
    abstract fun bindsMovieDetailViewModel(viewModel: MovieDetailViewModel): ViewModel
}