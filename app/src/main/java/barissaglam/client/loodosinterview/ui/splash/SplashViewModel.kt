package barissaglam.client.loodosinterview.ui.splash

import androidx.lifecycle.MutableLiveData
import barissaglam.client.loodosinterview.base.viewmodel.BaseViewModel
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import javax.inject.Inject

class SplashViewModel @Inject constructor(private val firebaseRemoteConfig: FirebaseRemoteConfig) : BaseViewModel() {
    val textRemoteConfig: MutableLiveData<String> = MutableLiveData()

    fun fetchTextFromFirebase() {
        firebaseRemoteConfig.fetchAndActivate().addOnCompleteListener {
            this.textRemoteConfig.value =
                if (it.isSuccessful) firebaseRemoteConfig.getString("splash_text")
                else "Remote Config Not Success"
        }
    }
}