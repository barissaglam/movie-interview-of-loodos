package barissaglam.client.loodosinterview.ui.detail

import android.app.Activity
import android.content.Intent
import android.view.MenuItem
import barissaglam.client.loodosinterview.R
import barissaglam.client.loodosinterview.base.view.BaseActivity
import barissaglam.client.loodosinterview.databinding.ActivityMovieDetailBinding

class MovieDetailActivity : BaseActivity<ActivityMovieDetailBinding, MovieDetailViewModel>() {
    override val layoutResourceId: Int = R.layout.activity_movie_detail
    override val classTypeOfViewModel: Class<MovieDetailViewModel> = MovieDetailViewModel::class.java

    override fun init() {
        super.init()
        baseView = binding.baseView
        binding.vm = viewModel
        setupToolbar()
    }

    override fun initStartRequest() {
        viewModel.getMovieDetail()
    }

    private fun setupToolbar() {
        setSupportActionBar(binding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    companion object {
        const val EXTRA_IMDB_ID = "barissaglam.client.loodosinterview.ui.detail.EXTRA_IMDB_ID"
        fun start(activity: Activity, imdbId: String?) {
            activity.startActivity(Intent(activity, MovieDetailActivity::class.java).apply {
                putExtra(EXTRA_IMDB_ID, imdbId)
            })
        }
    }
}