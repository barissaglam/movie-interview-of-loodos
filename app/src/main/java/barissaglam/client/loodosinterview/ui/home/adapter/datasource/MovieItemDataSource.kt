package barissaglam.client.loodosinterview.ui.home.adapter.datasource

import androidx.paging.PageKeyedDataSource
import barissaglam.client.loodosinterview.base.extension.applySchedulers
import barissaglam.client.loodosinterview.base.viewmodel.BaseViewModel
import barissaglam.client.loodosinterview.data.Movie
import barissaglam.client.loodosinterview.ui.home.HomeViewModel

class MovieItemDataSource(
    private val viewModel: HomeViewModel
) : PageKeyedDataSource<Int, Movie>() {

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, Movie>) {}

    override fun loadInitial(
        params: LoadInitialParams<Int>,
        callback: LoadInitialCallback<Int, Movie>
    ) {
        viewModel.mPage = 1
        viewModel.onFirstPageLoading()
        val manage = viewModel.apiDataManager.searchMovie(keyword=viewModel.searchText?:"", page = viewModel.mPage)
        viewModel.getCompositeDisposable().add(
            manage.applySchedulers()
                .subscribe({
                    if (it.movies.isNullOrEmpty()){
                        viewModel.baseLiveData.value = BaseViewModel.State.ShowEmpty
                    }else{
                        val nextPage = if (it.getTotalPage() > viewModel.mPage) viewModel.mPage else null
                        viewModel.loadMoreVisibility.value = nextPage != null
                        callback.onResult(it.movies, null, nextPage)
                        viewModel.setInit(it)
                        viewModel.onFirstPageLoaded()
                    }

                }, {
                    viewModel.baseLiveData.value = BaseViewModel.State.OnError(it, BaseViewModel.RequestType.INIT)
                }, {

                })
        )
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, Movie>) {
        viewModel.mPage = viewModel.mPage + 1
        val manage = viewModel.apiDataManager.searchMovie(keyword = viewModel.searchText?:"", page = viewModel.mPage)
        viewModel.getCompositeDisposable().add(
            manage.applySchedulers()
                .subscribe({
                    val nextPage = if (it.getTotalPage() > viewModel.mPage) viewModel.mPage else null
                    viewModel.loadMoreVisibility.value = nextPage != null
                    callback.onResult(it.movies ?: arrayListOf(), nextPage)
                    viewModel.onFirstPageLoaded()
                    viewModel.setInit(it)
                }, {
                    viewModel.baseLiveData.value = BaseViewModel.State.OnError(it, BaseViewModel.RequestType.INIT)
                }, {

                })
        )
    }


}

