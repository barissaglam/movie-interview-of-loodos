package barissaglam.client.loodosinterview.ui.home.adapter.viewholder

import androidx.lifecycle.MutableLiveData
import barissaglam.client.loodosinterview.base.adapter.BaseViewHolder
import barissaglam.client.loodosinterview.data.Movie
import barissaglam.client.loodosinterview.databinding.ItemMovieBinding
import barissaglam.client.loodosinterview.ui.home.adapter.HomeAdapter

class HomeViewHolder(val binding: ItemMovieBinding, private val liveData: MutableLiveData<HomeAdapter.State>) : BaseViewHolder<Movie>(binding.root) {
    override fun bind(data: Movie) {
        binding.movie = data
        binding.title.isSelected = true

        binding.content.setOnClickListener {
            liveData.value = HomeAdapter.State.OnMovieClick(data)
        }
    }

}