package barissaglam.client.loodosinterview.ui.home.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.MutableLiveData
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.GridLayoutManager
import barissaglam.client.loodosinterview.R
import barissaglam.client.loodosinterview.base.adapter.BaseViewHolder
import barissaglam.client.loodosinterview.data.Movie
import barissaglam.client.loodosinterview.databinding.ItemLoadMoreBinding
import barissaglam.client.loodosinterview.databinding.ItemMovieBinding
import barissaglam.client.loodosinterview.ui.home.adapter.viewholder.HomeViewHolder
import barissaglam.client.loodosinterview.ui.home.adapter.viewholder.LoadMoreViewHolder

class HomeAdapter(private val loadMoreVisibility: MutableLiveData<Boolean>) : PagedListAdapter<Movie, BaseViewHolder<*>>(diffCallback) {
    val liveData = MutableLiveData<State>()

    companion object {

        private val diffCallback = object : DiffUtil.ItemCallback<Movie>() {
            override fun areItemsTheSame(oldItem: Movie, newItem: Movie): Boolean = oldItem.title == newItem.title && oldItem.year == newItem.year
            override fun areContentsTheSame(oldItem: Movie, newItem: Movie): Boolean = oldItem == newItem
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<*> {
        val layoutInflater = LayoutInflater.from(parent.context)
        return ItemType.values()[viewType].onCreateViewHolder(parent, layoutInflater, liveData)
    }

    override fun getItemViewType(position: Int): Int {
        return if (itemCount > 1 && itemCount == position + 1) {
            ItemType.TYPE_LOAD_MORE.viewType()
        } else return ItemType.TYPE_MOVIE.viewType()
    }

    override fun onBindViewHolder(holder: BaseViewHolder<*>, position: Int) {
        when (holder) {
            is HomeViewHolder -> holder.bind(getItem(position) ?: Movie())
            is LoadMoreViewHolder -> holder.bind(loadMoreVisibility)
        }
    }

    val spanSizeLookupMulti = object : GridLayoutManager.SpanSizeLookup() {
        override fun getSpanSize(position: Int): Int {
            return if (getItemViewType(position) == ItemType.TYPE_LOAD_MORE.viewType()) {
                3
            } else {
                1
            }
        }
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    enum class ItemType {
        TYPE_MOVIE {
            override fun onCreateViewHolder(parent: ViewGroup, layoutInflater: LayoutInflater, liveData: MutableLiveData<State>): BaseViewHolder<*> {
                val binding = DataBindingUtil.inflate<ItemMovieBinding>(layoutInflater, R.layout.item_movie, parent, false)
                return HomeViewHolder(binding, liveData)
            }
        },
        TYPE_LOAD_MORE {
            override fun onCreateViewHolder(parent: ViewGroup, layoutInflater: LayoutInflater, liveData: MutableLiveData<State>): BaseViewHolder<*> {
                val binding = DataBindingUtil.inflate<ItemLoadMoreBinding>(layoutInflater, R.layout.item_load_more, parent, false)
                return LoadMoreViewHolder(binding)
            }
        };

        abstract fun onCreateViewHolder(parent: ViewGroup, layoutInflater: LayoutInflater, liveData: MutableLiveData<State>): BaseViewHolder<*>

        fun viewType(): Int = ordinal
    }

    sealed class State {
        data class OnMovieClick(val movie: Movie) : State()
    }
}