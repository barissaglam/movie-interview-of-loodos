package barissaglam.client.loodosinterview.ui.home.adapter.viewholder

import androidx.core.view.isVisible
import androidx.lifecycle.MutableLiveData
import barissaglam.client.loodosinterview.base.adapter.BaseViewHolder
import barissaglam.client.loodosinterview.databinding.ItemLoadMoreBinding

class LoadMoreViewHolder(
    val binding: ItemLoadMoreBinding
) : BaseViewHolder<MutableLiveData<Boolean>>(binding.root) {

    override fun bind(data: MutableLiveData<Boolean>) {
        binding.status = data
        binding.container.isVisible = data.value ?: false
    }
}