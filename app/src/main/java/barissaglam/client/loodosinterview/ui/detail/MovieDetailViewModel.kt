package barissaglam.client.loodosinterview.ui.detail

import android.os.Bundle
import androidx.databinding.ObservableField
import barissaglam.client.loodosinterview.api.ApiDataManager
import barissaglam.client.loodosinterview.base.viewmodel.BaseViewModel
import barissaglam.client.loodosinterview.data.response.MovieDetailResponse
import javax.inject.Inject

class MovieDetailViewModel @Inject constructor(private val apiDataManager: ApiDataManager) : BaseViewModel() {
    var imdbId: String? = null
    val detailResponse = ObservableField<MovieDetailResponse>()

    override fun handleIntent(extras: Bundle) {
        this.imdbId = extras.getString(MovieDetailActivity.EXTRA_IMDB_ID)
    }

    fun getMovieDetail() {
        apiDataManager.getMovieDetail(imdbId).sendRequest(requestType = RequestType.INIT, successHandler = {
            this.detailResponse.set(it)

        })
    }
}