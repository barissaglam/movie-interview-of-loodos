package barissaglam.client.loodosinterview.ui.splash

import android.app.ActivityOptions
import android.graphics.drawable.AnimationDrawable
import android.os.Handler
import android.util.Log
import android.util.Pair
import android.view.View
import android.view.animation.DecelerateInterpolator
import androidx.core.content.res.ResourcesCompat
import androidx.core.view.ViewCompat
import barissaglam.client.loodosinterview.R
import barissaglam.client.loodosinterview.base.extension.observe
import barissaglam.client.loodosinterview.base.view.BaseActivity
import barissaglam.client.loodosinterview.databinding.ActivitySplashBinding
import barissaglam.client.loodosinterview.ui.home.HomeActivity
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import javax.inject.Inject


class SplashActivity : BaseActivity<ActivitySplashBinding, SplashViewModel>() {
    override val layoutResourceId: Int = R.layout.activity_splash
    override val classTypeOfViewModel: Class<SplashViewModel> = SplashViewModel::class.java

    private val handler = Handler()

    override fun init() {
        super.init()

        viewModel.fetchTextFromFirebase()
        observe(viewModel.textRemoteConfig, ::onTextFetchedFromFirebase)

    }

    private fun onTextFetchedFromFirebase(textRemoteConfig: String) {
        binding.text.text = textRemoteConfig
        setupAnimationOfViews()
        handler.postDelayed({
            HomeActivity.start(this@SplashActivity, binding.imgLogo)
        }, 3000)
    }

    private fun setupAnimationOfViews() {
        (binding.imgLogo.drawable as AnimationDrawable).start()

        ViewCompat.animate(binding.imgLogo).translationY(-60f)
            .setDuration(1000.toLong())
            .setInterpolator(DecelerateInterpolator(1.2f))
            .start()

        ViewCompat.animate(binding.text)
            .alpha(1f)
            .setStartDelay(1000.toLong())
            .setInterpolator(DecelerateInterpolator(1.2f))
            .setDuration(1000)
            .start()
    }

    override fun onPause() {
        super.onPause()
        handler.removeCallbacksAndMessages(null)
        (binding.imgLogo.drawable as AnimationDrawable).stop()
    }

    override fun onStop() {
        super.onStop()
        supportFinishAfterTransition()
    }
}
