package barissaglam.client.loodosinterview.ui.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import barissaglam.client.loodosinterview.api.ApiDataManager
import barissaglam.client.loodosinterview.base.viewmodel.BaseViewModel
import barissaglam.client.loodosinterview.data.Movie
import barissaglam.client.loodosinterview.data.response.SearchResponse
import barissaglam.client.loodosinterview.ui.home.adapter.datasource.MovieItemDataSource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

class HomeViewModel @Inject constructor(val apiDataManager: ApiDataManager) : BaseViewModel() {
    val loadMoreVisibility = MutableLiveData<Boolean>()
    var searchText: String? = null
    var mPage: Int = 1
    var dataSource: MovieItemDataSource? = null
    var searchResponseLiveData = MutableLiveData<SearchResponse>()

    fun getItemPageList(): LiveData<PagedList<Movie>> {
        val config = PagedList.Config.Builder()
            .setPageSize(10)
            .setInitialLoadSizeHint(10)
            .setPrefetchDistance(2)
            .setEnablePlaceholders(false)
            .build()
        return initializedPagedListBuilder(config).build()
    }

    private fun initializedPagedListBuilder(config: PagedList.Config): LivePagedListBuilder<Int, Movie> {
        val dataSourceFactory = object : DataSource.Factory<Int, Movie>() {
            override fun create(): DataSource<Int, Movie> {
                dataSource = MovieItemDataSource(this@HomeViewModel)
                return dataSource!!
            }
        }
        return LivePagedListBuilder<Int, Movie>(dataSourceFactory, config)
    }


    fun setInit(searchResponse: SearchResponse) {
        this.searchResponseLiveData.value = searchResponse
        baseLiveData.value = State.ShowContent(RequestType.INIT)
    }

    fun onFirstPageLoaded() {
        baseLiveData.value = State.ShowContent(RequestType.INIT)
    }

    fun onFirstPageLoading() {
        GlobalScope.launch {
            withContext(Dispatchers.Main) {
                baseLiveData.value = State.ShowLoading(RequestType.INIT)
            }
        }
    }

}