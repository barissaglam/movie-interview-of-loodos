package barissaglam.client.loodosinterview.ui.home

import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.view.View
import android.widget.TextView
import androidx.appcompat.widget.SearchView
import androidx.core.app.ActivityOptionsCompat
import androidx.core.util.Pair
import androidx.core.view.ViewCompat
import androidx.recyclerview.widget.GridLayoutManager
import barissaglam.client.loodosinterview.R
import barissaglam.client.loodosinterview.base.extension.observe
import barissaglam.client.loodosinterview.base.view.BaseActivity
import barissaglam.client.loodosinterview.data.Movie
import barissaglam.client.loodosinterview.databinding.ActivityHomeBinding
import barissaglam.client.loodosinterview.ui.detail.MovieDetailActivity
import barissaglam.client.loodosinterview.ui.home.adapter.HomeAdapter


class HomeActivity : BaseActivity<ActivityHomeBinding, HomeViewModel>() {
    override val layoutResourceId: Int = R.layout.activity_home
    override val classTypeOfViewModel: Class<HomeViewModel> = HomeViewModel::class.java

    lateinit var homeAdapter: HomeAdapter

    override fun init() {
        super.init()
        baseView = binding.baseView
        setupRecyclerView()
        setupSearchView()

    }

    private fun setupRecyclerView() {
        homeAdapter = HomeAdapter(viewModel.loadMoreVisibility)
        homeAdapter.setHasStableIds(true)
        observe(homeAdapter.liveData, ::onHomeAdapterStateChanged)
        binding.recyclerView.adapter = homeAdapter
        binding.recyclerView.setHasFixedSize(true)
        (binding.recyclerView.layoutManager as GridLayoutManager).spanSizeLookup = homeAdapter.spanSizeLookupMulti
    }

    private fun onHomeAdapterStateChanged(state: HomeAdapter.State) {
        when (state) {
            is HomeAdapter.State.OnMovieClick -> onMovieClick(state.movie)
        }
    }

    private fun onMovieClick(movie: Movie) {
        MovieDetailActivity.start(this, movie.imdbID)
    }

    private fun setupSearchView() {
        (binding.searchView.findViewById(androidx.appcompat.R.id.search_src_text) as TextView).setTextColor(Color.WHITE)
        binding.searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                if (query.isNullOrEmpty()) return false

                viewModel.searchText = query
                viewModel.onFirstPageLoading()
                observe(viewModel.getItemPageList()) {
                    if (this@HomeActivity::homeAdapter.isInitialized) homeAdapter.submitList(it)
                }
                binding.searchView.clearFocus()
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                return true
            }
        })
    }

    override fun onStop() {
        super.onStop()
        binding.logo.clearAnimation()
    }


    companion object {
        fun start(activity: Activity, transitionView: View) {
            val options = ActivityOptionsCompat.makeSceneTransitionAnimation(activity, Pair(transitionView, ViewCompat.getTransitionName(transitionView)))
            activity.startActivity(Intent(activity, HomeActivity::class.java), options.toBundle())
        }
    }
}