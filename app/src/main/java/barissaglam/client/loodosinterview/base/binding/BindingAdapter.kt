package barissaglam.client.loodosinterview.base.binding


import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.databinding.BindingAdapter
import androidx.databinding.DataBindingUtil
import barissaglam.client.loodosinterview.R
import barissaglam.client.loodosinterview.data.Rating
import barissaglam.client.loodosinterview.databinding.ItemRatingBinding
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions


@BindingAdapter("visibility")
fun setVisibility(view: View, value: Boolean) {
    view.visibility = if (value) View.VISIBLE else View.GONE
}

@BindingAdapter("imageUrl")
fun setImageUrl(imageView: ImageView, url: String?) {
    if (url.isNullOrEmpty()) return

    val requestOptions = RequestOptions().transform(CenterCrop(), RoundedCorners(32))
    Glide
        .with(imageView.context)
        .load(url)
        .placeholder(R.drawable.placeholder)
        .diskCacheStrategy(DiskCacheStrategy.ALL)
        .apply(requestOptions)
        .into(imageView)
}

@BindingAdapter("prepareRating")
fun prepareRating(linearLayout: LinearLayout, ratingList: ArrayList<Rating>?) {
    if (ratingList.isNullOrEmpty()) return

    for (rating in ratingList) {
        val item = DataBindingUtil.inflate<ItemRatingBinding>(LayoutInflater.from(linearLayout.context), R.layout.item_rating, null, false)
        item.rating = rating
        linearLayout.addView(item.root)
    }


}

