package barissaglam.client.loodosinterview.base.view

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.Window
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.ViewModelProvider
import barissaglam.client.loodosinterview.R
import barissaglam.client.loodosinterview.base.extension.getErrorMessage
import barissaglam.client.loodosinterview.base.extension.observe
import barissaglam.client.loodosinterview.base.viewmodel.BaseViewModel
import barissaglam.client.loodosinterview.view.BaseView
import com.afollestad.materialdialogs.MaterialDialog
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import javax.inject.Inject

abstract class BaseActivity<DB : ViewDataBinding, VM : BaseViewModel> : AppCompatActivity(), HasAndroidInjector {
    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Any>
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    override fun androidInjector(): AndroidInjector<Any> = dispatchingAndroidInjector

    @get:LayoutRes
    protected abstract val layoutResourceId: Int
    lateinit var binding: DB
    lateinit var viewModel: VM
    protected abstract val classTypeOfViewModel: Class<VM>
    private var hasRequestSend = false
    private var progressDialog: Dialog? = null
    protected var baseView: BaseView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel = ViewModelProvider(this, viewModelFactory).get(classTypeOfViewModel)
        binding = DataBindingUtil.setContentView(this, layoutResourceId)

        intent?.extras?.let {
            viewModel.handleIntent(it)
            it.clear()
        }

        setUpViewModelStateObservers()

        init()

        if (!hasRequestSend) {
            initStartRequest()
            hasRequestSend = true
        }
    }

    open fun initStartRequest() {}
    open fun init() {}

    private fun setUpViewModelStateObservers() {
        observe(viewModel.baseLiveData, ::onStateChanged)
    }

    private fun onStateChanged(state: BaseViewModel.State) {
        when (state) {
            is BaseViewModel.State.ShowLoading -> showLoading(state.requestType)
            is BaseViewModel.State.ShowContent -> showContent(state.requestType)
            is BaseViewModel.State.ShowEmpty -> baseView?.showEmpty()
            is BaseViewModel.State.OnError -> showError(state.throwable, state.requestType)
        }
    }

    private fun showLoading(requestType: BaseViewModel.RequestType?) {
        when (requestType) {
            BaseViewModel.RequestType.ACTION -> showProgress()
            else -> baseView?.showLoading()
        }
    }

    private fun showContent(requestType: BaseViewModel.RequestType?) {
        when (requestType) {
            BaseViewModel.RequestType.ACTION -> dismissProgress()
            else -> baseView?.showContent()
        }
    }

    private fun showError(throwable: Throwable, requestType: BaseViewModel.RequestType) {
        val dialogMessage = throwable.getErrorMessage(this)
        when (requestType) {
            BaseViewModel.RequestType.INIT -> generateInitRequestError(dialogMessage)
            BaseViewModel.RequestType.ACTION -> generateActionRequestError(
                dialogMessage,
                requestType
            )
            else -> {
            }
        }
    }

    private fun generateActionRequestError(
        dialogMessage: String,
        requestType: BaseViewModel.RequestType
    ) {
        onStateChanged(BaseViewModel.State.ShowContent(requestType))
        MaterialDialog(this).show {
            message(text = dialogMessage)
            positiveButton(text = "Anladım")
        }.title(text = "Uyarı")
    }

    private fun generateInitRequestError(dialogMessage: String) {
        baseView?.showContent()
        MaterialDialog(this).show {
            message(text = dialogMessage)
            cancelable(false)
            positiveButton(text = "Yeniden Dene") { initStartRequest() }
            negativeButton(text = "Kapat") { onBackPressed() }
        }
    }

    private fun showProgress(dialogDim: Float? = null) {
        if (progressDialog == null) {
            progressDialog = Dialog(this).apply {
                requestWindowFeature(Window.FEATURE_NO_TITLE)
                setCancelable(false)
                setContentView(R.layout.dialog_progress)
                window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                dialogDim?.let { window?.setDimAmount(it) }
            }
        }
        if (progressDialog?.isShowing == false) progressDialog?.show()
    }

    private fun dismissProgress() {
        progressDialog?.let {
            if (it.isShowing) {
                it.dismiss()
            }
        }
    }

    override fun onDestroy() {
        dismissProgress()
        super.onDestroy()
    }
}