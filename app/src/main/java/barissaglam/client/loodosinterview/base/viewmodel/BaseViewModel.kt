package barissaglam.client.loodosinterview.base.viewmodel

import android.os.Bundle
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

abstract class BaseViewModel(
    private val compositeDisposable: CompositeDisposable = CompositeDisposable(),
    val baseLiveData: MutableLiveData<State> = MutableLiveData()
) : ViewModel() {
    open fun handleIntent(extras: Bundle) {}
    fun getCompositeDisposable() = compositeDisposable
    fun Disposable.track() {
        compositeDisposable.add(this)
    }


    override fun onCleared() {
        compositeDisposable.clear()
    }

    inline fun <T> Observable<T>.sendRequest(
        requestType: RequestType = RequestType.CUSTOM,
        crossinline successHandler: (T) -> Unit
    ) {
        baseLiveData.value = State.ShowLoading(requestType)
        subscribe(
            {
                successHandler(it)
                baseLiveData.value = State.ShowContent(requestType)
            },
            {
                baseLiveData.value = State.OnError(it, requestType)
            })
            .track()
    }


    sealed class State {
        data class OnError(val throwable: Throwable, val requestType: RequestType) : State()
        data class ShowLoading(val requestType: RequestType? = null) : State()
        data class ShowContent(val requestType: RequestType? = null) : State()
        object ShowEmpty : State()
    }

    enum class StateType {
        DEFAULT, // Bütün stateleri base gönderir
        NONE,  // Hiç bir stateyi base e göndermez
        ONLY_START_PROGRESS  // Sadece showLoadingi base e gönderir
    }

    enum class RequestType {
        INIT,
        ACTION,
        CUSTOM
    }

}