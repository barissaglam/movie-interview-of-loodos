package barissaglam.client.loodosinterview.view

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.AnimationDrawable
import android.util.AttributeSet
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.isVisible
import barissaglam.client.loodosinterview.R
import com.airbnb.lottie.LottieAnimationView


class BaseView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    var progress: ImageView
    var container: LinearLayout
    var lottie: LottieAnimationView
    var empty: TextView

    init {
        View.inflate(context, R.layout.view_base, this)

        progress = findViewById(R.id.progress)
        container = findViewById(R.id.view_base_container)
        lottie = findViewById(R.id.lottie_view)
        empty = findViewById(R.id.empty)
        container.setBackgroundColor(Color.TRANSPARENT)

        attrs.let {
            val a = context.obtainStyledAttributes(attrs, R.styleable.BaseView)
            setEmptyText(a.getString(R.styleable.BaseView_emptyText) ?: "")

            a.recycle()
        }
    }

    private fun setEmptyText(text: String) {
        empty.text = text
    }

    fun showLoading() {
        progress.isVisible = true
        (progress.drawable as AnimationDrawable).start()
        lottie.isVisible = false
        empty.isVisible = false
        setHelperChildVisibility(false)
        lottie.pauseAnimation()
    }

    fun showContent() {
        setHelperChildVisibility(true)
        progress.isVisible = false
        (progress.drawable as AnimationDrawable).stop()
        lottie.isVisible = false
        empty.isVisible = false
        lottie.pauseAnimation()
    }

    fun showEmpty() {
        progress.isVisible = false
        (progress.drawable as AnimationDrawable).stop()
        lottie.isVisible = false
        empty.isVisible = true
        lottie.pauseAnimation()
    }

    /*
        Kullanıcı bir arama yaptıktan sonra tekrar arama yaptığında progress görünmesi için Activity'deki kullanılan BaseView'in childi gizlenmesi gerekiyor.
        Bu fonksiyon bu amaçla yazıldı.
     */
    private fun setHelperChildVisibility(state: Boolean) {
        for (i in 0 until this.childCount) {
            if (getChildAt(i) != container)
                getChildAt(i).isVisible = state
        }
    }

}