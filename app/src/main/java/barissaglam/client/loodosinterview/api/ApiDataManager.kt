package barissaglam.client.loodosinterview.api

import barissaglam.client.loodosinterview.data.response.MovieDetailResponse
import barissaglam.client.loodosinterview.data.response.SearchResponse
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ApiDataManager @Inject constructor(private val apiService: ApiService) {


    fun searchMovie(keyword: String, page:Int): Observable<SearchResponse> {
        return apiService.searchMovie(keyword,page).applySchedulers()
    }

    fun getMovieDetail(imdbId: String?): Observable<MovieDetailResponse> {
        return apiService.getMovieDetail(imdbId ?: "").applySchedulers()
    }


    fun <T> Observable<T>.applySchedulers(): Observable<T> {
        return subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
    }

}