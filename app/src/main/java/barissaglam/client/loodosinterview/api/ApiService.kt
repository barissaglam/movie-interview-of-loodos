package barissaglam.client.loodosinterview.api

import barissaglam.client.loodosinterview.data.response.MovieDetailResponse
import barissaglam.client.loodosinterview.data.response.SearchResponse
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {

    @GET("/")
    fun searchMovie(@Query("s") keyword: String, @Query("page") page: Int): Observable<SearchResponse>

    @GET("/")
    fun getMovieDetail(@Query("i") imdbId: String): Observable<MovieDetailResponse>
}